package ictgradschool.industry.abstraction.farmmanager.animals;

public class Duck extends Animal
    {
    private final int MAX_VALUE = 400;

    public Duck()
        {
        value = 300;
        }

    @Override
    public void feed()
        {
        if (value < MAX_VALUE) {
            value = value + 25;
        }
        }

    @Override
    public int costToFeed()
        {
        return 5;
        }

    @Override
    public String getType()
        {
        return "Duck";
        }

    @Override
    public String toString()
        {
        return getType() + " - $" + value;
        }
    }
